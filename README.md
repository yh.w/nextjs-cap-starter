# NextJs with Capacitor Starter

## Dependencies

 - [NextJs](https://github.com/vercel/next.js/)
 - [Typescript](https://github.com/microsoft/TypeScript)
 - [Capacitor](https://github.com/ionic-team/capacitor)
 - [Tailwind](https://github.com/tailwindlabs/tailwindcss)
 - [Jest](https://github.com/facebook/jest)

## Base Steps - NextJs & Capacitor

1. Creating the Next App

```bash
yarn create next-app --ts
```

2. Installing Capacitor && init

```bash
# Installs
yarn add @capacitor/core @capacitor/android @capacitor/ios
yarn add @capacitor/cli -D

# Init
npx cap init --web-dir=out # to specify directory of compiled web assets
```

3. Add plaforms **‼ before run ‼**

```bash
npx cap add android
npx cap add ios
```

4. Build & export & sync, wen assets. Scripts can be added into `package.json`

```bash
next build
next export
npx cap sync
```

5. Open & run

```bash
# Open Xcode or Android Studio
npx cap open ios
npx cap open android

# Run on simulators
npx cap run ios
npx cap run android
```

## Tailwind

See official tutorial https://tailwindcss.com/docs/guides/nextjs.

## Jest

1. Install packages

```bash
yarn add jest @testing-library/react @testing-library/jest-dom -D
```

2. Create `jest.config.js`

```js
// jest.config.js
const nextJest = require("next/jest");
const createJestConfig = nextJest({
  dir: "./",
});
const customJestConfig = {
  moduleDirectories: ["node_modules", "<rootDir>/"],
  testEnvironment: "jest-environment-jsdom",
};
module.exports = createJestConfig(customJestConfig);
```

3. Run test script

```bash
jest
```

4. Test files, under `__tests__/`

```js
// Example: __tests__/index.test.tsx
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react'
import Home from '../pages/index'

describe('Home', () => {
  it('renders a heading', () => {
    render(<Home />)

    const heading = screen.getByRole('heading', {
      name: /welcome to next\.js!/i,
    })

    expect(heading).toBeInTheDocument();
  })
})
```
